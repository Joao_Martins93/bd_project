TABELAS

CREATE TABLE Tripulação(nome char(30), 
	id integer(30) PRIMARY KEY,
	idade integer, 
	sexo char(30), 
	morada char(30),
    	categoria char(30),
    	senioridade char(30)  
);

CREATE TABLE pessoal_cabine(
    	nome char(30), 
	id integer(30) PRIMARY KEY REFERENCES tripulacao(id),
	idade integer, 
	sexo char(30), 
	morada char(30),
    	categoria char(30),
    	senioridade char(30)
);


CREATE TABLE chefe_cabine(
    	nome char(30), 
	id integer PRIMARY KEY REFERENCES tripulacao(id),
	idade integer, 
	sexo char(30), 
	morada char(30),
    	categoria char(30),
    	senioridade char(30)
);


CREATE TABLE pilotos(
    	nome char(30), 
	id integer(30) PRIMARY KEY REFERENCES tripulacao(id),
	idade integer, 
	sexo char(30), 
	morada char(30),
    	categoria char(30),
    	senioridade char(30),
    	horas_voo integer,
    	aterragens integer,
    	licenca integer,
    	data_emissao DATE   	
);


CREATE TABLE comandante(
    	nome char(30), 
	id integer PRIMARY KEY REFERENCES tripulacao(id),
	idade integer, 
	sexo char(30), 
	morada char(30),
    	categoria char(30),
    	senioridade char(30),
    	horas_comando integer,
    	data_promocao DATE
);


CREATE TABLE avioes(
    	matricula char(30) PRIMARY KEY,
    	nome char(30), 
	data_aquisicao DATE
);


CREATE TABLE tipo_aviao(
    	id_aviao integer(30) PRIMARY KEY,
    	marca char(30), 
    	n_tripulacao integer,
    	modelo char(30),
    	carga integer,
    	n_passageiros integer,
    	milhas integer,
    	horas_voo integer
);

CREATE TABLE manutencoes(
    	id_manutencao integer(30) PRIMARY KEY,
    	data_manutencao DATE, 
    	n_milhas integer,
    	horas_voo integer,
    	situacoes_verificadas char(50),
    	n_aterragens integer
);


CREATE TABLE outsoursing(
    	id_out integer(30) PRIMARY KEY,
    	empresa char(30), 
    	data_entrega DATE,
    	tempo_manutencao integer,
    	preco float
);


CREATE TABLE rota(
    	id_rota integer(30) PRIMARY KEY,
    	cidade_inicio char(30),
    	cidade_fim char(30),
    	milhas integer
);


CREATE TABLE aeroporto(
    	id_aeroporto integer(30) PRIMARY KEY,
    	cidade char(30)
);


CREATE TABLE escalas(
    	id_escala integer(30) PRIMARY KEY,
    	codigo_aeroporto char(30),
    	local char(30),
    	país char(30)
);


CREATE TABLE voo(
    	id_voo integer(30) PRIMARY KEY,
    	planeamento char(30),
    	data_partida DATE,
    	data_chegada DATE,
    	hora_partida integer,
    	hora_chegada integer
);



CREATE TABLE cancelado(
    	id_cancelamento integer(30) PRIMARY KEY,
    	id_responsavel char(30),
    	razoes char(60)
);


----------------------------------------------------------


CREATE TABLE piloto_aviao(
id integer(30),
id_aviao integer(30),
PRIMARY KEY (id, id_aviao),
FOREIGN KEY (id) REFERENCES pilotos (id),
FOREIGN KEY (id_aviao) REFERENCES tipo_aviao (id_aviao)
);


CREATE TABLE piloto_voo(
id integer(30),
id_voo integer(30),
PRIMARY KEY (id, id_voo),
FOREIGN KEY (id) REFERENCES pilotos (id),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo)
);



CREATE TABLE comandante_voo(
id integer(30),
id_voo integer(30),
PRIMARY KEY (id, id_voo),
FOREIGN KEY (id) REFERENCES comandante (id),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo)
);



CREATE TABLE cabine_voo(
id integer(30),
id_voo integer(30),
PRIMARY KEY (id, id_voo),
FOREIGN KEY (id) REFERENCES pessoal_cabine (id),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo)
);




CREATE TABLE tripulação_voo(
id integer(30),
id_voo integer(30),
PRIMARY KEY (id, id_voo),
FOREIGN KEY (id) REFERENCES tripulação (id),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo)
);



CREATE TABLE chefe_cabine_pessoal(
id_chef integer(30),
id_cabine integer(30),
PRIMARY KEY (id_chef, id_cabine),
FOREIGN KEY (id_cabine) REFERENCES pessoal_cabine (id),
FOREIGN KEY (id_chef) REFERENCES chefe_cabine (id)
);








CREATE TABLE tipo_aviao_rota(
id_aviao integer(30),
id_rota integer(30),
PRIMARY KEY (id_aviao, id_rota),
FOREIGN KEY (id_aviao) REFERENCES tipo_aviao (id_aviao),
FOREIGN KEY (id_rota) REFERENCES rota (id_rota)
);




CREATE TABLE tipo_aviao_avioes(
id_aviao integer(30),
matricula char(30),
PRIMARY KEY (id_aviao, matricula),
FOREIGN KEY (id_aviao) REFERENCES tipo_aviao (id_aviao),
FOREIGN KEY (matricula) REFERENCES avioes (matricula)
);



CREATE TABLE avioes_manutencao(
id_manutencao integer(30),
matricula char(30),
PRIMARY KEY (id_manutencao, matricula),
FOREIGN KEY (id_manutencao) REFERENCES manutencoes (id_manutencao),
FOREIGN KEY (matricula) REFERENCES avioes (matricula)
);



CREATE TABLE avioes_voo(
id_voo integer(30),
matricula char(30),
PRIMARY KEY (id_voo, matricula),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo),
FOREIGN KEY (matricula) REFERENCES avioes (matricula)
);



CREATE TABLE manutencao_outsoursing(
id_manutencao integer(30),
id_out integer(30),
PRIMARY KEY (id_manutencao, id_out),
FOREIGN KEY (id_manutencao) REFERENCES manutencoes (id_manutencao),
FOREIGN KEY (id_out) REFERENCES outsoursing (id_out)
);




CREATE TABLE rota_aeroporto(
id_rota integer(30),
id_aeroporto integer(30),
PRIMARY KEY (id_rota, id_aeroporto),
FOREIGN KEY (id_rota) REFERENCES rota (id_rota),
FOREIGN KEY (id_aeroporto) REFERENCES aeroporto (id_aeroporto)
);



CREATE TABLE rota_voo(
id_rota integer(30),
id_voo integer(30),
PRIMARY KEY (id_rota, id_voo),
FOREIGN KEY (id_rota) REFERENCES rota (id_rota),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo)
);




CREATE TABLE rota_escalas(
id_rota integer(30),
id_escala integer(30),
ordem integer(30),
PRIMARY KEY (id_rota, id_escala),
FOREIGN KEY (id_rota) REFERENCES rota (id_rota),
FOREIGN KEY (id_escala) REFERENCES escalas (id_escala)
);




CREATE TABLE voo_cancelamento(
id_voo integer(30),
id_cancelamento integer(30),
PRIMARY KEY (id_voo, id_cancelamento),
FOREIGN KEY (id_voo) REFERENCES voo (id_voo),
FOREIGN KEY (id_cancelamento) REFERENCES cancelado (id_cancelamento)
);



------------------------------------------------------------
VALORES

INSERT 
INTO tripulação(nome, id, idade, sexo, morada, categoria, senioridade)
VALUES 
('João', 1, 27, 'M', 'Rua 123', 'A', 'Comandante'),
('ANA', 2, 24, 'F', 'Rua 321', 'B', 'Piloto'),
('Pedro', 3, 27, 'M', 'Rua 213', 'C', 'Chefe de cabine'),
('Miguel', 4, 26, 'M', 'Rua 312', 'D', 'Pessoal cabine'),
('Luís', 5, 28, 'M', 'Rua 421', 'D', 'Pessoal cabine'),
('Andreia', 6, 25, 'F', 'Rua 654', 'D', 'Pessoal cabine'),
('Maria', 7, 30, 'F', 'Avenida 876', 'D', 'Pessoal cabine'),
('Filipe', 8, 29, 'M', 'Praçeta 654', 'D', 'Pessoal cabine'),
('Nuno', 9, 35, 'M', 'Avenida 123', 'D', 'Pessoal cabine'),
('Beatriz', 10, 30, 'F', 'Rua 987', 'D', 'Pessoal cabine');




INSERT 
INTO comandante(nome, id, idade, sexo, morada, categoria, senioridade, horas_comando, data_promocao)
VALUES 
('João', 11, 27, 'M', 'Rua 987', 'A', 'Comandante', 3000, 19900403),
('RUI', 12, 24, 'M', 'Rua 789', 'B', 'Comandante', 2000, 19900403),
('Maria', 13, 27, 'F', 'Rua 897', 'C', 'Comandante', 1000, 19900403),
('Joana', 14, 26, 'F', 'Rua 879', 'D', 'Comandante', 300, 19900403),
('Bruno', 15, 28, 'M', 'Rua 876', 'D', 'Comandante', 200, 19900403),
('Rita', 16, 25, 'F', 'Rua 678', 'D', 'Comandante', 100, 19900403),
('Miguel', 17, 30, 'M', 'Avenida 786', 'D', 'Comandante', 1234, 19900403),
('Filipe', 18, 29, 'M', 'Praçeta 768', 'D', 'Comandante', 1432, 19900403),
('Pedro', 19, 35, 'M', 'Avenida 765', 'D', 'Comandante', 1323, 19900403),
('Ana', 20, 30, 'F', 'Rua 567', 'D', 'Comandante', 1111, 19900403);




INSERT 
INTO pilotos(nome, id, idade, sexo, morada, categoria, senioridade, horas_voo, aterragens, licenca, data_emissao)
VALUES 
('João', 21, 27, 'M', 'Rua 987', 'A', 'Piloto', 3000, 1000, 999999, 19900403),
('RUI', 22, 24, 'M', 'Rua 789', 'B', 'Piloto', 2000, 500, 888888, 19900403),
('Maria', 23, 27, 'F', 'Rua 897', 'C', 'Piloto', 1000, 400, 777777, 19900403),
('Joana', 24, 26, 'F', 'Rua 879', 'D', 'Piloto', 2000, 500, 666666, 19900403),
('Bruno', 25, 28, 'M', 'Rua 876', 'D', 'Piloto', 2000, 500, 555555, 19900403),
('Rita', 26, 25, 'F', 'Rua 678', 'D', 'Piloto', 2000, 500, 444444, 19900403),
('Miguel', 27, 30, 'M', 'Avenida 786', 'D', 'Piloto', 2000, 500, 333333, 19900403),
('Filipe', 28, 29, 'M', 'Praçeta 768', 'D', 'Piloto', 2000, 500, 222222, 19900403),
('Pedro', 29, 35, 'M', 'Avenida 765', 'D', 'Piloto', 2000, 500, 111111, 19900403),
('Ana', 30, 30, 'F', 'Rua 567', 'D', 'Piloto', 2000, 500, 123456, 19900403);





INSERT 
INTO avioes(matricula, nome, data_aquisicao	)
VALUES 
('AAA', 'A', 19900403),
('BBB', 'A', 19900403),
('CCC', 'A', 19900403),
('DDD', 'A', 19900403),
('EEE', 'A', 19900403),
('FFF', 'A', 19900403),
('GGG', 'A', 19900403),
('HHH', 'A', 19900403),
('III', 'A', 19900403),
('JJJ', 'A', 19900403);




INSERT 
INTO tipo_aviao(id_aviao, marca, n_tripulacao, modelo, carga, n_passageiros, milhas, horas_voo)
VALUES 
(1, 'A', 1000, 'AA', 5000000, 2000, 7000, 24),
(2, 'B', 900, 'BB', 4000000, 2000, 7000, 24),
(3, 'C', 900, 'CC', 4000000, 2000, 7000, 24),
(4, 'D', 1000, 'DD', 5000000, 2000, 7000, 24),
(5, 'E', 1000, 'EE', 5000000, 2000, 7000, 24),
(6, 'F', 800, 'FF', 3000000, 2000, 7000, 24),
(7, 'F', 800, 'FF', 3000000, 2000, 7000, 24),
(8, 'A', 1000, 'AA', 5000000, 2000, 7000, 24),
(9, 'B', 1000, 'BB', 5000000, 2000, 7000, 24),
(10, 'C', 1000, 'CC', 5000000, 2000, 7000, 24);




INSERT 
INTO manutencoes(id_manutencao, data_manutencao, n_milhas, horas_voo, situacoes_verificadas, n_aterragens)
VALUES 
(1, 20010723, 5000, 2500, '5', 4),
(2, 20010724, 900, 2000, '2', 2),
(3, 20010725, 900, 1700, '4', 4),
(4, 20010726, 1000, 2009, '2', 1),
(5, 20010727, 1000, 1242, '7', 5),
(6, 20010728, 800, 1111, '9', 8),
(7, 20010713, 800, 125, '3', 5),
(8, 20010714, 1000, 765, '6', 7),
(9, 20010715, 1000, 999, '3', 1),
(10, 20010716, 1000, 853, '4', 4);





INSERT 
INTO outsoursing(id_out, empresa, data_entrega, tempo_manutencao, preco)
VALUES 
(1, 'AA', 20180308, 30, 3000.40),
(2, 'BB', 20180309, 20, 2000.40),
(3, 'CC', 20180310, 42, 4020.40),
(4, 'DD', 20180311, 24, 2040.40),
(5, 'EE', 20180312, 34, 3040.40),
(6, 'FF', 20180313, 45, 4050.40),
(7, 'GG', 20180314, 43, 4030.40),
(8, 'HH', 20180315, 21, 2010.40),
(9, 'II', 20180316, 13, 1030.40),
(10, 'JJ', 20180317, 14, 1040.40);




INSERT 
INTO rota(id_rota, cidade_inicio, cidade_fim, milhas)
VALUES 
(1, 'AA', 'KK', 30),
(2, 'BB', 'LL', 20),
(3, 'CC', 'OO', 42),
(4, 'DD', 'MM', 24),
(5, 'EE', 'NN', 34),
(6, 'FF', 'PP', 45),
(7, 'GG', 'QQ', 43),
(8, 'HH', 'RR', 21),
(9, 'II', 'SS', 13),
(10, 'JJ', 'TT', 14);




INSERT 
INTO aeroporto(id_aeroporto, cidade)
VALUES 
(1, 'AA'),
(2, 'BB'),
(3, 'CC'),
(4, 'DD'),
(5, 'EE'),
(6, 'FF'),
(7, 'GG'),
(8, 'HH'),
(9, 'II'),
(10, 'JJ');




INSERT 
INTO escalas(id_escala, codigo_aeroporto, local, país)
VALUES 
(1, 'AA', 'AA', 'AA'),
(2, 'BB', 'BB', 'BB'),
(3, 'CC', 'CC', 'CC'),
(4, 'DD', 'DD', 'DD'),
(5, 'EE', 'EE', 'EE'),
(6, 'FF', 'FF', 'FF'),
(7, 'GG', 'GG', 'GG'),
(8, 'HH', 'HH', 'HH'),
(9, 'II', 'II', 'II'),
(10, 'JJ', 'JJ', 'JJ');






INSERT 
INTO voo(id_voo, planeamento, data_partida, data_chegada, hora_partida, hora_chegada)
VALUES 
(1, 'Planeado', 20190201, 20190202, 17, 02),
(2, 'Realizado', 20190203, 20190204, 06, 17),
(3, 'Em curso', 20190204, 20190204, 09, 15),
(4, 'Planeado', 20190207, 20190208, 19, 05),
(5, 'Realizado', 20190202, 20190202, 17, 23),
(6, 'Em curso', 20190205, 20190205, 17, 21),
(7, 'Planeado', 20190208, 20190208, 06, 20),
(8, 'Realizado', 20190201, 20190202, 08, 18),
(9, 'Em curso', 20190206, 20190207, 10, 20),
(10, 'Planeado', 20190215, 20190216, 23, 10);
