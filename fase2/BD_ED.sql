-- 1 -> Comandantes que realizaram voos com destino a Paris entre 31/10/2015 e 30/11/2015.

SELECT DISTINCT t.id, t.nome, t.apelido, t.tipo 
FROM tripulante t, voo v, rota r, piloto p 
where r.cod_aeroporto_fim = 'CDG' and (t.id = 25100 or t.id = 27900) and v.data_partida BETWEEN '2015-10-31' and '2015-11-30'

ou

SELECT DISTINCT t.id, t.nome, t.apelido, t.tipo 
FROM tripulante t, voo v, rota r, piloto p 
where r.cod_aeroporto_fim = 'CDG' and p.tipo= 'comandante' and p.id = t.id and v.data_partida BETWEEN '2015-10-31' and '2015-11-30'


-------------------
selecionar comandates da tripulação

SELECT * from piloto p, tripulante t where p.tipo= 'comandante' and p.id = t.id

-------------------


-- 2 ->  Número total de voos efetuados por cada avião desde o início do presente ano

SELECT a.nome, COUNT(*)
from voo v, aviao a
where v.matricula = a.matricula
GROUP BY a.matricula



-- 3 -> Voos em que o comandante Abel Antunes e o copiloto Carlos Caldas voaram juntos.

SELECT *
from voo v
where v.id_comandante = 25100 and v.id_copiloto = 18200



-- 4 -> Comandantes (nome completo, n.º licença e data de emissão da licença) habilitados a
pilotar aviões cuja autonomia seja superior a 500km. Pretende-se que o resultado seja
ordenado alfabeticamente, por nome próprio e por apelido, respetivamente.

SELECT DISTINCT t.nome, t.apelido, h.n_licenca, h.data_licenca 
from habilitado h, piloto p, tripulante t, tipoaviao ta 
where h.id = p.id and h.cod_tipo = ta.cod_tipo and ta.autonomia > 500 and (p.tipo= 'comandante' and p.id = t.id)
ORDER BY t.nome, t.apelido



-- 5 -> Pilotos que nunca realizaram voos da rota 12345.

SELECT DISTINCT t.id, t.nome, t.apelido, t.tipo
from tripulante t, piloto p
where t.id = p.id
EXCEPT(
SELECT DISTINCT t.id, t.nome, t.apelido, t.tipo
from voo v, rota r, tripulante t
where v.cod_rota = r.cod_rota and v.cod_rota = 12345 and (t.id = v.id_comandante or t.id = v.id_copiloto))



-- 6 -> Aviões que já efetuaram voos em todas as rotas da companhia.




-- 7 -> Nome e n.º de horas de voo dos copilotos que fizeram o maior número de voos.
Pretende-se saber o n.º exato de voos feitos por cada um desses copilotos.

SELECT t.nome, p.n_horas_voo , COUNT(*)
from tripulante t, piloto p, voo v 
where p.id = t.id and p.tipo = 'copiloto' and v.id_copiloto = p.id
GROUP BY p.id




-- 8 -> Voos que permitem viagens de Lisboa a Paris. Note que devem ser considerados
também os voos que contenham escalas nestas duas cidades.

SELECT * 
from voo v, rota r, escala e
where v.cod_rota = r.cod_rota and r.cod_aeroporto_ini='LIS' and r.cod_rota= e.cod_rota and e.cod_aeroporto= 'CDG'
UNION(
SELECT  *, null, null, null 
from voo v, rota r  
where v.cod_rota = r.cod_rota and r.cod_aeroporto_ini='LIS' and r.cod_aeroporto_fim='CDG')



-- 9 

SELECT DISTINCT r.cod_rota AS Rota , r.cod_aeroporto_ini AS Aeroporto_Partida, a.local AS Cidade, 
r.cod_aeroporto_fim AS Aeroporto_Chegada, a.local AS Cidade, e.cod_aeroporto AS Escala, a.local AS Cidade, e.n_ordem AS Ordem
FROM rota r, aeroporto a, escala e
WHERE r.cod_aeroporto_ini = a.cod_aeroporto AND r.cod_rota = e.cod_rota AND (r.cod_rota = 12346 OR r.cod_rota = 12347 OR r.cod_rota = 12352 OR r.cod_rota = 12353) AND (a.cod_aeroporto = 'LIS' OR a.cod_aeroporto = 'HND' OR a.cod_aeroporto = 'BKK')




